import supertest from 'supertest';
import app from '../src/app';
import appErrors from '../src/appErrors';

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'dev';
}

interface ResultInterface {
  server: any;
  request: any;
}

// let server: any;
let request: any;

export default () => {
  const result: ResultInterface = {
    server: undefined,
    request: undefined
  };
  if (request) {
    result.request = request;
  } else {
    appErrors(app);
    result.request = request = supertest(app);
  }
  return result;
};