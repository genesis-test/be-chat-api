import * as chai from 'chai';
const expect = chai.expect;
import faker from 'faker/locale/ru';
import * as GlobalContext from '../../../src/globalContext';
import * as InfrastructureContext from '../../../src/infrastructureContext';
import * as ChatContext from '../../../src/chatContext';
import initServer from '../../initServer';

export default () => {
  let request: any;

  beforeAll(async () => {
    const result = initServer();
    request = result.request;
  });

  describe('user cases 1: TODO', () => {
    beforeAll(async () => {
      // TODO create test data
    });

    test('TODO: test', done => {
      request
        .post(`/auth/signup`)
        .set('Accept', 'application/json')
        .send({
          // TODO data to send
        })
        .expect(200)
        .end(async (err: Error, res: any) => {
          if (err) return done(err);
          expect(res.body).have.not.property('errors');
          const bodyObj = res.body;
          expect(bodyObj).have.not.null;
          expect(bodyObj).have.property('status').and.be.false;
          expect(bodyObj).have.property('data');
          // TODO check rules
          done();
        });
    });

    afterAll(async () => {
      // TODO remove test data
    });
  });
};