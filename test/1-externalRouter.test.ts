import * as chai from 'chai';
const expect = chai.expect;
import initTest from './initTest';
import initServer from './initServer';
import * as initConnection from './initConnection';
import loginPOST from './externalRouter/auth/loginPOST';
import loginTokenPOST from './externalRouter/auth/loginTokenPOST';
import signUpPOST from './externalRouter/auth/signUpPOST';

beforeAll(async () => {
  initTest();
  initServer();
  return await initConnection.connection();
});

describe('externalRouter.loginPOST', loginPOST);
describe('externalRouter.loginTokenPOST', loginTokenPOST);
describe('externalRouter.signUpPOST', signUpPOST);

afterAll(() => initConnection.disconnect());