import * as InfrastructureContext from '../src/infrastructureContext';
import installFirstData from '../src/installFirstData';

let lowdbService: any;

export const lowdbServiceClass = () => lowdbService;

export const connection = async () => {
  lowdbService = new InfrastructureContext.LowdbService();
  await lowdbService.connect({
    path: process.env.NODE_ENV === 'production' ? '/tmp' : `${__dirname}/..`,
    db: 'db.json'
  });
  // Install first data
  await installFirstData();
};

export const disconnect = () => {
  return new Promise((resolve) => {
    // Logic for disconnect
    resolve();
  });
};