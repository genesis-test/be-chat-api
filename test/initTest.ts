import * as dotenv from 'dotenv';

export default () => {
  if (!process.env.NODE_ENV) {
    process.env.NODE_ENV = 'dev';
  }
  process.env.TESTING = 'integration';
  dotenv.config({ path: `${__dirname}/../.env.local` });
  dotenv.config({ path: `${__dirname}/../.env.dev` });
  process.env.TZ = 'Etc/UTC';
};