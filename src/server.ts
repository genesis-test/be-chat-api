import http from 'http';
import * as dotenv from 'dotenv';
import socketIO from 'socket.io';
import appErrors from './appErrors';
import authMiddleware from './externalWebsocket/authMiddleware';
import connectEvent from './externalWebsocket/connectEvent';
import installFirstData from './installFirstData';
import * as GlobalContext from './globalContext';
import * as InfrastructureContext from './infrastructureContext';
import * as ChatContext from './chatContext';

/**
 * Variables
 */
process.env.TZ = 'Etc/UTC';
if (!process.env.NODE_ENV) process.env.NODE_ENV = 'dev';
let server: http.Server;
let websocket: InfrastructureContext.WebsocketServerType;

/**
 * Read constants
 * .env.local — global for all environments
 * .env.dev — only for dev environment
 */
dotenv.config({ path: `${__dirname}/../.env.local` });
if (process.env.NODE_ENV === 'dev') dotenv.config({ path: `${__dirname}/../.env.dev` });

/**
 * Import app, after variables!
 */
import app from './app';

/**
 * Initialize lowDB storage
 */
const lowdbService = new InfrastructureContext.LowdbService();

/**
 * Start block
 */
try {
  (async () => {
    // Connect to DB
    await lowdbService.connect({
      path: process.env.NODE_ENV === 'production' ? '/tmp' : `${__dirname}/..`,
      db: 'db.json'
    });
    // Install first data
    await installFirstData();
    // Add express error handlers
    appErrors(app);
    // Create server
    server = http.createServer(app);
    // Create websocket server
    websocket = socketIO.listen(server);
    // Save websocket server to global cache
    InfrastructureContext.WebsocketService.websocketSet(websocket);
    // Auth middleware
    websocket.use(authMiddleware);
    // User connect
    websocket.on('connection', connectEvent);
    // Start server
    server.listen({ port: process.env.PORT }, async () => {
      if ( process.send ) { process.send('ready'); }
    });
  })();
} catch (err) {
  // TODO Logger for errors
  console.error(err);
}

/**
 * Shutdown block
 */
const onShutdown = async () => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const roomService = new ChatContext.RoomService(errorClass);
  await roomService.disconnectAllUser();
  await GlobalContext.delay(300);
  websocket.close();
  server.close();
  process.exit();
};
process.on('SIGTERM', onShutdown);
process.on('SIGINT', onShutdown);