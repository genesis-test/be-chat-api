import { v4 as uuidv4 } from 'uuid';
import * as GlobalTypes from '../globalTypes';

export function uuid4(): GlobalTypes.UUID {
  return uuidv4();
}