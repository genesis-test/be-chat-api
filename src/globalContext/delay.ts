export function delay(time: number): Promise<void> {
  return new Promise((resolve: any) => {
    setTimeout(() => resolve(), time);
  });
}