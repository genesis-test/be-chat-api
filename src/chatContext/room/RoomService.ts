import * as GlobalContext from '../../globalContext';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat Room manipulation
 * @class
 */
export class RoomService {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create room
   * @param {string} userId1
   * @param {string} userId2
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async create(
    userId1: string,
    userId2: string
  ): Promise<ChatContext.RoomObjInterface> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    const insertObj: ChatContext.RoomObjInterface = {
      id: GlobalContext.uuid4(),
      userId1,
      userId2,
      connect1: true,
      connect2: false
    };
    await roomRepository.create(insertObj);
    if (this._errorClass.error) return;
    return insertObj;
  }

  /**
   * Update room
   * @param {string} id
   * @param {object} updateObj
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async update(
    id: string,
    updateObj: object
  ): Promise<ChatContext.RoomObjInterface> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    const messageRes = await roomRepository.update(id, updateObj);
    if (this._errorClass.error) return;
    return messageRes;
  }

  /**
   * Find room by userIds
   * @param {string} userId1
   * @param {string} userId2
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async findByUserIds(userId1: string, userId2: string): Promise<ChatContext.RoomObjInterface> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    const roomsRes = await Promise.all([
      roomRepository.findByObj({'userId1': userId1, 'userId2': userId2}),
      roomRepository.findByObj({'userId1': userId2, 'userId2': userId1}),
    ]);
    if (this._errorClass.error) return;
    return roomsRes.find((roomRes: ChatContext.RoomObjInterface) => !!roomRes) || null;
  }

  /**
   * Check bot for userIds
   * @param {string} userId1
   * @param {string} userId2
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async findBotByUserIds(userId1: string, userId2: string): Promise<ChatContext.RoomObjInterface> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    const roomsRes = await Promise.all([
      roomRepository.findByObj({'userId1': userId1, 'userId2': userId2}),
      roomRepository.findByObj({'userId1': userId2, 'userId2': userId1}),
    ]);
    if (this._errorClass.error) return;
    return roomsRes.find((roomRes: ChatContext.RoomObjInterface) => !!roomRes) || null;
  }

  /**
   * User disconnect
   * @param {string} userId
   * @returns {Promise<ChatContext.RoomObjInterface[]>}
   */
  public async disconnectUser(userId: string): Promise<ChatContext.RoomObjInterface[]> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);

    const roomsAllRes = await Promise.all([
      roomRepository.findMany({'userId1': userId, 'connect1': true}, 'id', 1000),
      roomRepository.findMany({'userId2': userId, 'connect2': true}, 'id', 1000)
    ]);
    if (this._errorClass.error) return;

    const promisesArr: Promise<ChatContext.RoomObjInterface>[] = [];
    roomsAllRes.forEach((roomsRes: ChatContext.RoomObjInterface[]) => {
      roomsRes.forEach((roomRes: ChatContext.RoomObjInterface) => {
        const updateObj: ChatContext.RoomSetType = {};
        if (roomRes.userId1 === userId) {
          updateObj.connect1 = false;
        }
        if (roomRes.userId2 === userId) {
          updateObj.connect2 = false;
        }
        promisesArr.push( this.update(roomRes.id, updateObj) );
      });
    });
    return await Promise.all(promisesArr);
  }

  /**
   * User connect
   * @param {string} userId1
   * @param {string} userId2
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async connectUser(userId1: string, userId2: string): Promise<ChatContext.RoomObjInterface> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    const roomsRes = await Promise.all([
      roomRepository.findByObj({'userId1': userId1, 'userId2': userId2}),
      roomRepository.findByObj({'userId1': userId2, 'userId2': userId1}),
    ]);
    if (this._errorClass.error) return;
    // Find first room
    const roomRes = roomsRes.find((roomRes: ChatContext.RoomObjInterface) => !!roomRes);

    if (roomRes) {
      const updateObj: ChatContext.RoomSetType = {};
      if (roomRes.userId1 === userId1) {
        updateObj.connect1 = true;
      }
      if (roomRes.userId2 === userId1) {
        updateObj.connect2 = true;
      }
      return await this.update(roomRes.id, updateObj);
    } else {
      return await this.create(userId1, userId2);
    }
  }

  /**
   * All users disconnect
   * @returns {Promise<boolean>}
   */
  public async disconnectAllUser(): Promise<boolean> {
    if (this._errorClass.error) return;
    const roomRepository = new ChatContext.RoomRepository(this._errorClass);
    await Promise.all([
      roomRepository.updateManyByObj({'connect1': true}, {'connect1': false}),
      roomRepository.updateManyByObj({'connect2': true}, {'connect2': false})
    ]);
    return true;
  }
}