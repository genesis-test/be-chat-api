import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat Room manipulation with db
 * @class
 */
export class RoomRepository {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create doc if not exists
   * @param {ChatContext.RoomObjInterface} insertObj
   * @returns {Promise<boolean>}
   */
  public async create(insertObj: ChatContext.RoomObjInterface): Promise<boolean> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      const itemRes = collection.find({id: insertObj.id}).value();
      if (!itemRes) await collection.push(insertObj).write();
      return true;
    } catch (err) {
      this._errorClass.setError('RoomRepository.create', 'error in push data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'create',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Update doc if exists
   * @param {string} id
   * @param {ChatContext.RoomSetType} updateObj
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async update(id: string, updateObj: ChatContext.RoomSetType): Promise<ChatContext.RoomObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      const itemRes = await collection
        .find({id})
        .assign(updateObj)
        .write();
      return itemRes;
    } catch (err) {
      this._errorClass.setError('RoomRepository.create', 'error in update data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'update',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Update doc by object
   * @param {object} findObj
   * @param {ChatContext.RoomSetType} updateObj
   * @returns {Promise<boolean>}
   */
  public async updateManyByObj(findObj: object, updateObj: ChatContext.RoomSetType): Promise<boolean> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      const itemsRes = <ChatContext.RoomObjInterface[]>await collection
        .filter(findObj)
        .value();

      const promisesArr: Promise<any>[] = [];
      itemsRes.forEach((itemRes: ChatContext.RoomObjInterface) => {
        promisesArr.push(
          collection.find({id: itemRes.id}).assign(updateObj).write()
        );
      });
      await Promise.all(promisesArr);
      return true;
    } catch (err) {
      this._errorClass.setError('RoomRepository.create', 'error in update data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'updateManyByObj',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by id
   * @param {string}
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async findById(id: string): Promise<ChatContext.RoomObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      return <ChatContext.RoomObjInterface>collection.find({id}).value();
    } catch (err) {
      this._errorClass.setError('RoomRepository.findById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'findById',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by object
   * @param {object} findObj
   * @returns {Promise<ChatContext.RoomObjInterface>}
   */
  public async findByObj(findObj: object): Promise<ChatContext.RoomObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      return <ChatContext.RoomObjInterface>collection.find(findObj).value();
    } catch (err) {
      this._errorClass.setError('RoomRepository.findById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'findByObj',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find many docs by custom query
   * @param {object} findObj
   * @returns {Promise<ChatContext.RoomObjInterface[]>}
   */
  public async findMany(findObj: object, sort: string, limit: number = 10): Promise<ChatContext.RoomObjInterface[]> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ROOMS_COLLECTION);
      return <ChatContext.RoomObjInterface[]>collection.filter(findObj).sortBy(sort).take(limit).value();
    } catch (err) {
      this._errorClass.setError('RoomRepository.findMany', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'RoomRepository',
        'logger.method': 'findMany',
        'logger.error': err
      });
      return;
    }
  }
}