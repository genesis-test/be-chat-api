import * as GlobalTypes from '../../globalTypes';

export interface RoomObjInterface {
  id: GlobalTypes.UUID;
  userId1: string;
  userId2: string;
  connect1: boolean;
  connect2: boolean;
}

export type RoomSetType = GlobalTypes.OptionalizeType<RoomObjInterface>;