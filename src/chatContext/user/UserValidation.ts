import * as GlobalContext from '../../globalContext';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';
import Joi from '@hapi/joi';

/**
 * Chat user validation
 * @class
 */
export class UserValidation {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Validate data for user create
   * @param {string} email
   * @param {string} password
   * @param {string} userName
   * @returns {Promise<ChatContext.UserObjInterface>}
   */
  public createUserFields(
    email: string,
    password: string,
    userName: string
  ) {
    const joiSchemaObj = {
      email: Joi.string().email().required().messages({
        'string.type': 'Invalid type, must be a email',
        'string.empty': 'Please enter email'
      }),
      password: Joi.string().required().messages({
        'string.type': 'Invalid type, must be a string',
        'string.empty': 'Please enter password'
      }),
      userName: Joi.string().required().messages({
        'string.type': 'Invalid type, must be a string',
        'string.empty': 'Please enter user name'
      })
    };
    const validateRes = Joi.object(joiSchemaObj).validate({email, password, userName});

    if (validateRes.error) {
      this._errorClass.setError('validateError', validateRes.error.details[0].message, '');
      return false;
    }
    return true;
  }

  /**
   * Validate data for user login
   * @param {string} email
   * @param {string} password
   * @returns {Promise<ChatContext.UserObjInterface>}
   */
  public loginUserFields(
    email: string,
    password: string
  ) {
    const joiSchemaObj = {
      email: Joi.string().email().required().messages({
        'string.type': 'Invalid type, must be a email',
        'string.empty': 'Please enter email'
      }),
      password: Joi.string().required().messages({
        'string.type': 'Invalid type, must be a string',
        'string.empty': 'Please enter password'
      })
    };
    const validateRes = Joi.object(joiSchemaObj).validate({email, password});

    if (validateRes.error) {
      this._errorClass.setError('validateError', validateRes.error.details[0].message, '');
      return false;
    }
    return true;
  }
}