import * as GlobalTypes from '../../globalTypes';
import * as ChatContext from '..';

export interface UserObjInterface {
  id: GlobalTypes.UUID;
  email: string;
  password: string;
  userName: string;
  avatar: string;
  description: string;
  isBot: boolean;
}

export interface UserAPIInterface {
  id: GlobalTypes.UUID;
  email: string;
  userName: string;
  avatar: string;
  description: string;
  isBot: boolean;
  online?: boolean;
  lastMessage?: ChatContext.MessageObjInterface;
}

export interface UserForListInterface {
  id: GlobalTypes.UUID;
  userName: string;
  avatar: string;
  message: string;
  online?: boolean;
}