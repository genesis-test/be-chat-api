import crypto from 'crypto';
import * as GlobalContext from '../../globalContext';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat User manipulation
 * @class
 */
export class UserService {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Generate password with salt
   * @param {string} password
   * @returns {string}
   */
  public generatePassword(password: string): string {
    return crypto.createHmac('sha256', process.env.AUTH_PASSWORD_SECRET)
      .update(password)
      .digest('hex');
  }

  /**
   * Create bot
   * @param {string} id
   * @param {string} userName
   * @param {string} avatar
   * @param {string} description
   * @returns {Promise<ChatContext.UserObjInterface>}
   */
  public async createBot(
    id: string,
    userName: string,
    avatar: string,
    description: string
  ): Promise<ChatContext.UserObjInterface> {
    if (this._errorClass.error) return;
    const userRepository = new ChatContext.UserRepository(this._errorClass);

    const insertObj: ChatContext.UserObjInterface = {
      id,
      email: null,
      password: null,
      userName,
      avatar,
      description,
      isBot: true
    };
    await userRepository.create(insertObj);
    if (this._errorClass.error) return;

    return insertObj;
  }

  /**
   * Validate user before create
   * @param {string} email
   * @param {string} password
   * @param {string} userName
   * @returns {Promise<boolean>}
   */
  public async createValidate(
    email: string,
    password: string,
    userName: string
  ): Promise<boolean>  {
    if (this._errorClass.error) return;
    const userValidation = new ChatContext.UserValidation(this._errorClass);
    const userRepository = new ChatContext.UserRepository(this._errorClass);
    // Validate data
    userValidation.createUserFields(email, password, userName);
    if (this._errorClass.error) return;
    // Check email
    const emailCheckRes = await userRepository.find({email});
    if (this._errorClass.error) return;
    if (emailCheckRes) {
      this._errorClass.setError('email', 'email is busy', '');
      return false;
    }
    // Check user name
    const userNameCheckRes = await userRepository.find({userName});
    if (this._errorClass.error) return;
    if (userNameCheckRes) {
      this._errorClass.setError('userName', 'user name is busy', '');
      return false;
    }
    // if all good
    return true;
  }

  /**
   * Validate user before create
   * @param {string} email
   * @param {string} password
   * @returns {Promise<boolean>}
   */
  public async loginValidate(
    email: string,
    password: string
  ): Promise<boolean>  {
    if (this._errorClass.error) return;
    const userValidation = new ChatContext.UserValidation(this._errorClass);
    // Validate data
    userValidation.loginUserFields(email, password);
    if (this._errorClass.error) return;
    return true;
  }

  /**
   * Create user
   * @param {string} email
   * @param {string} password
   * @param {string} userName
   * @returns {Promise<ChatContext.UserAPIInterface>}
   */
  public async create(
    email: string,
    password: string,
    userName: string
  ): Promise<ChatContext.UserAPIInterface> {
    if (this._errorClass.error) return;
    const userRepository = new ChatContext.UserRepository(this._errorClass);

    const insertObj: ChatContext.UserObjInterface = {
      id: GlobalContext.uuid4(),
      email,
      password: this.generatePassword(password),
      userName,
      avatar: null,
      description: null,
      isBot: false
    };
    await userRepository.create(insertObj);
    if (this._errorClass.error) return;
    return {
      id: insertObj.id,
      email: insertObj.email,
      userName: insertObj.userName,
      avatar: insertObj.avatar,
      description: insertObj.description,
      isBot: insertObj.isBot
    };
  }

  /**
   * Find user by id
   * @param {string} id
   * @returns {Promise<ChatContext.UserAPIInterface>}
   */
  public async findById(id: string): Promise<ChatContext.UserAPIInterface> {
    if (this._errorClass.error) return;
    const userRepository = new ChatContext.UserRepository(this._errorClass);
    const userObj = await userRepository.findById(id);
    if (this._errorClass.error) return;
    return {
      id: userObj.id,
      email: userObj.email,
      userName: userObj.userName,
      avatar: userObj.avatar,
      description: userObj.description,
      isBot: userObj.isBot
    };
  }

  /**
   * Find user by email and password
   * @param {string} email
   * @param {string} password
   * @returns {Promise<ChatContext.UserAPIInterface>}
   */
  public async findByEmailAndPassword(email: string, password: string): Promise<ChatContext.UserAPIInterface> {
    if (this._errorClass.error) return;
    const userRepository = new ChatContext.UserRepository(this._errorClass);
    const userObj = await userRepository.find({
      email,
      password: this.generatePassword(password)
    });
    if (this._errorClass.error) return;
    if (!userObj) {
      this._errorClass.setError('emailOrPasswordIncorrect', 'email or password incorrect', '');
      return;
    }
    return {
      id: userObj.id,
      email: userObj.email,
      userName: userObj.userName,
      avatar: userObj.avatar,
      description: userObj.description,
      isBot: userObj.isBot
    };
  }

  /**
   * Find all users
   * @returns {Promise<ChatContext.UserAPIInterface[]>}
   */
  public async findAll(): Promise<ChatContext.UserAPIInterface[]> {
    if (this._errorClass.error) return;
    const userRepository = new ChatContext.UserRepository(this._errorClass);
    const usersRes = await userRepository.findMany({}, 'name');
    if (this._errorClass.error) return;
    return usersRes.map((userRes: ChatContext.UserObjInterface) => {
      return {
        id: userRes.id,
        email: userRes.email,
        userName: userRes.userName,
        avatar: userRes.avatar,
        description: userRes.description,
        isBot: userRes.isBot
      };
    });
  }


}