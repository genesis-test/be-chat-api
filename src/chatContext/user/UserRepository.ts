import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat User manipulation with db
 * @class
 */
export class UserRepository {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create doc if not exists
   * @param {ChatContext.UserObjInterface} insertObj
   * @returns {Promise<boolean>}
   */
  public async create(insertObj: ChatContext.UserObjInterface): Promise<boolean> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_USERS_COLLECTION);
      const itemRes = collection.find({id: insertObj.id}).value();
      if (!itemRes) await collection.push(insertObj).write();
      return true;
    } catch (err) {
      this._errorClass.setError('UserRepository.create', 'error in push data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'UserRepository',
        'logger.method': 'create',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by id
   * @param {string} id
   * @returns {Promise<ChatContext.UserObjInterface>}
   */
  public async findById(id: string): Promise<ChatContext.UserObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_USERS_COLLECTION);
      return <ChatContext.UserObjInterface>collection.find({id}).value();
    } catch (err) {
      this._errorClass.setError('UserRepository.findById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'UserRepository',
        'logger.method': 'findById',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by custom query
   * @param {object} findObj
   * @returns {Promise<ChatContext.UserObjInterface>}
   */
  public async find(findObj: object): Promise<ChatContext.UserObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_USERS_COLLECTION);
      return <ChatContext.UserObjInterface>collection.find(findObj).value();
    } catch (err) {
      this._errorClass.setError('UserRepository.find', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'UserRepository',
        'logger.method': 'find',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find many docs by custom query
   * @param {object} findObj
   * @param {string} sort
   * @returns {Promise<ChatContext.UserObjInterface[]>}
   */
  public async findMany(findObj: object, sort: string): Promise<ChatContext.UserObjInterface[]> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_USERS_COLLECTION);
      return <ChatContext.UserObjInterface[]>collection.filter(findObj).sortBy(sort).value();
    } catch (err) {
      this._errorClass.setError('UserRepository.findMany', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'UserRepository',
        'logger.method': 'findMany',
        'logger.error': err
      });
      return;
    }
  }
}