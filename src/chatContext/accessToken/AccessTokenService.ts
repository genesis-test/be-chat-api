import jwt from 'jsonwebtoken';
import * as GlobalContext from '../../globalContext';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat AccessToken manipulation
 * @class
 */
export class AccessTokenService {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create token string
   * @param {string} userId
   * @returns {string}
   */
  public createToken(userId: string): string {
    return jwt.sign({ userId }, process.env.AUTH_JWT_SECRET, {
      expiresIn: process.env.AUTH_JWT_EXPIRES_IN,
      jwtid: GlobalContext.uuid4()
    });
  }

  /**
   * Verify token and decode
   * @param {string} token
   * @returns {ChatContext.AccessTokenJWTInterface}
   */
  public verifyToken(token: string): ChatContext.AccessTokenJWTInterface {
    try {
      return <ChatContext.AccessTokenJWTInterface>jwt.verify(token, process.env.AUTH_JWT_SECRET);
    } catch (err) {
      if (err.name === 'TokenExpiredError') {
        return {
          error: true,
          ...<ChatContext.AccessTokenJWTInterface>jwt.verify(token, process.env.AUTH_JWT_SECRET, {ignoreExpiration: true})
        };
      }
      InfrastructureContext.LoggerService.error({
        'logger.type': 'class_method',
        'logger.className': 'AccessTokenService',
        'logger.method': 'verifyToken',
        'logger.error': err
      });
      this._errorClass.setError('verifyToken', err.message, '');
      return;
    }
  }

  /**
   * Create jwt token and save in db
   * @param {string} userId
   * @returns {Promise<ChatContext.AccessTokenObjInterface>}
   */
  public async create(userId: string): Promise<ChatContext.AccessTokenObjInterface> {
    if (this._errorClass.error) return;
    const accessTokenRepository = new ChatContext.AccessTokenRepository(this._errorClass);
    // create token
    const token: string = this.createToken(userId);
    if (this._errorClass.error) return;
    // verify token
    const tokenObj: ChatContext.AccessTokenJWTInterface = this.verifyToken(token);
    if (this._errorClass.error) return;
    if (tokenObj.error) return;
    // save data for token
    const insertObj: ChatContext.AccessTokenObjInterface = {
      id: tokenObj.jti,
      expiredAt: tokenObj.exp * 1000,
      userId: tokenObj.userId
    };
    await accessTokenRepository.create(insertObj);
    return Object.assign({token}, insertObj);
  }

  /**
   * Validate jwt token and return obj
   * @param {string} token
   * @returns {Promise<ChatContext.AccessTokenObjInterface>}
   */
  public async validate(token: string): Promise<ChatContext.AccessTokenObjInterface> {
    if (this._errorClass.error) return;
    const accessTokenRepository = new ChatContext.AccessTokenRepository(this._errorClass);
    // verify token
    const tokenObj: ChatContext.AccessTokenJWTInterface = this.verifyToken(token);
    if (this._errorClass.error) return;
    if (tokenObj.error) return;
    const tokenRes = await accessTokenRepository.findById(tokenObj.jti);
    if (!tokenRes) return;
    // delete if expired
    if (Date.now() > tokenRes.expiredAt) {
      await accessTokenRepository.deleteById(tokenObj.jti);
      return;
    }
    return tokenRes;
  }
}