import * as GlobalTypes from '../../globalTypes';

export interface AccessTokenObjInterface {
  id: string;
  expiredAt: number;
  userId: string;
  token?: string;
}

export interface AccessTokenJWTInterface {
  error?: boolean;
  userId: string;
  exp: number;
  iat: number;
  jti: string;
}