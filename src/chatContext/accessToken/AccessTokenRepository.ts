import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat AccessToken manipulation with db
 * @class
 */
export class AccessTokenRepository {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create doc if not exists
   * @param {ChatContext.AccessTokenObjInterface} insertObj
   * @returns {Promise<boolean>}
   */
  public async create(insertObj: ChatContext.AccessTokenObjInterface): Promise<boolean> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ACCESS_TOKENS_COLLECTION);
      const itemRes = collection.find({id: insertObj.id}).value();
      if (!itemRes) await collection.push(insertObj).write();
      return true;
    } catch (err) {
      this._errorClass.setError('AccessTokenRepository.create', 'error in push data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'AccessTokenRepository',
        'logger.method': 'create',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by id
   * @param {string}
   * @returns {Promise<ChatContext.AccessTokenObjInterface>}
   */
  public async findById(id: string): Promise<ChatContext.AccessTokenObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ACCESS_TOKENS_COLLECTION);
      return <ChatContext.AccessTokenObjInterface>collection.find({id}).value();
    } catch (err) {
      this._errorClass.setError('AccessTokenRepository.findById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'AccessTokenRepository',
        'logger.method': 'findById',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Delete doc by id
   * @param {string}
   * @returns {Promise<ChatContext.AccessTokenObjInterface>}
   */
  public async deleteById(id: string): Promise<ChatContext.AccessTokenObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_ACCESS_TOKENS_COLLECTION);
      return await <ChatContext.AccessTokenObjInterface>collection.remove({id}).write();
    } catch (err) {
      this._errorClass.setError('AccessTokenRepository.deleteById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'AccessTokenRepository',
        'logger.method': 'deleteById',
        'logger.error': err
      });
      return;
    }
  }
}