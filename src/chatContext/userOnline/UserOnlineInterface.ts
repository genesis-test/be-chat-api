export interface UsersOnlineObjInterface {
  [key: string]: UserOnlineObjInterface;
}
export interface UserOnlineObjInterface {
  userId: string;
  socketId: string;
}