import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

const usersHash: ChatContext.UsersOnlineObjInterface = {};

/**
 * Chat User Online manipulation
 * @class
 */
export class UserOnlineService {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Return connect user hash
   * @returns {ChatContext.RoomConnectsObjInterface}
   */
  public get usersHash(): ChatContext.UsersOnlineObjInterface {
    return usersHash;
  }

  /**
   * Connect user
   * @param {string} userId
   * @param {string} socketId
   * @returns {ChatContext.RoomUserConnectObjInterface}
   */
  public connectUser(
    userId: string,
    socketId: string
  ): ChatContext.UserOnlineObjInterface {
    usersHash[userId] = {
      userId,
      socketId
    };
    return usersHash[userId];
  }

  /**
   * Disconnect user
   * @param {string} userId
   */
  public disconnectUser(userId: string): void {
    if (usersHash[userId]) {
      delete usersHash[userId];
    }
  }
}