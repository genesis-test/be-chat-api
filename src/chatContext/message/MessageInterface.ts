import * as GlobalTypes from '../../globalTypes';

export interface MessageObjInterface {
  id: GlobalTypes.UUID;
  roomId: string;
  userId: string;
  message: string;
  createdAt: number;
  seenAt: number;
}

export type MessageSetType = GlobalTypes.OptionalizeType<MessageObjInterface>;