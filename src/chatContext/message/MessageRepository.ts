import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat Message manipulation with db
 * @class
 */
export class MessageRepository {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create doc if not exists
   * @param {ChatContext.MessageObjInterface} insertObj
   * @returns {Promise<boolean>}
   */
  public async create(insertObj: ChatContext.MessageObjInterface): Promise<boolean> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_MESSAGES_COLLECTION);
      const itemRes = collection.find({id: insertObj.id}).value();
      if (!itemRes) await collection.push(insertObj).write();
      return true;
    } catch (err) {
      this._errorClass.setError('MessageRepository.create', 'error in push data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'MessageRepository',
        'logger.method': 'create',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Update doc if exists
   * @param {string} id
   * @param {ChatContext.MessageSetType} updateObj
   * @returns {Promise<ChatContext.MessageObjInterface>}
   */
  public async update(id: string, updateObj: ChatContext.MessageSetType): Promise<ChatContext.MessageObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_MESSAGES_COLLECTION);
      const itemRes = await collection
        .find({id})
        .assign(updateObj)
        .write();
      return itemRes;
    } catch (err) {
      this._errorClass.setError('MessageRepository.update', 'error in update data to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'MessageRepository',
        'logger.method': 'update',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find doc by id
   * @param {string} id
   * @returns {Promise<ChatContext.MessageObjInterface>}
   */
  public async findById(id: string): Promise<ChatContext.MessageObjInterface> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_MESSAGES_COLLECTION);
      return <ChatContext.MessageObjInterface>collection.find({id}).value();
    } catch (err) {
      this._errorClass.setError('MessageRepository.findById', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'MessageRepository',
        'logger.method': 'findById',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Find many docs by custom query
   * @param {object} findObj
   * @param {string} [sort=undefined]
   * @param {number} [limit=0]
   * @param {number} [watermark=undefined]
   * @returns {Promise<ChatContext.MessageObjInterface[]>}
   */
  public async findMany(findObj: object, sort: string = undefined, limit: number = 0, watermark: number = undefined): Promise<ChatContext.MessageObjInterface[]> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_MESSAGES_COLLECTION);
      let cursor = collection.filter(findObj);
      if (watermark) {
        cursor = cursor.filter((message: ChatContext.MessageObjInterface) => {
          return (message.createdAt < watermark);
        });
      }
      if (sort) cursor.sortBy(sort);
      if (limit) cursor.take(limit);
      return <ChatContext.MessageObjInterface[]>cursor.value();
    } catch (err) {
      this._errorClass.setError('MessageRepository.findMany', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'MessageRepository',
        'logger.method': 'findMany',
        'logger.error': err
      });
      return;
    }
  }

  /**
   * Count messages
   * @param {object} findObj
   * @param {number} [watermark=undefined]
   * @returns {Promise<number>}
   */
  public async count(findObj: object, watermark: number = undefined): Promise<number> {
    try {
      const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
      const collection: any = lowDB.get(process.env.LOWDB_MESSAGES_COLLECTION);
      if (watermark) {
        return <number>collection.filter((message: ChatContext.MessageObjInterface) => {
          return (message.createdAt < watermark);
        }).size().value();
      }
      return <number>collection.filter(findObj).size().value();
    } catch (err) {
      this._errorClass.setError('MessageRepository.count', 'error in request to lowdb', '', err);
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'MessageRepository',
        'logger.method': 'count',
        'logger.error': err
      });
      return;
    }
  }
}