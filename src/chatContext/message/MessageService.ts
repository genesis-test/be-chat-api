import * as GlobalContext from '../../globalContext';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '..';

/**
 * Chat Message manipulation
 * @class
 */
export class MessageService {
  /**
   * @param {InfrastructureContext.ErrorClassEntity} _errorClass
   */
  protected _errorClass: InfrastructureContext.ErrorClassEntity;

  /**
   * @param {InfrastructureContext.ErrorClassEntity} errorClass
   */
  constructor(errorClass: InfrastructureContext.ErrorClassEntity) {
    this._errorClass = errorClass;
  }

  /**
   * Create message
   * @param {string} roomId
   * @param {string} userId
   * @param {string} message
   * @returns {Promise<ChatContext.MessageObjInterface>}
   */
  public async create(
    roomId: string,
    userId: string,
    message: string
  ): Promise<ChatContext.MessageObjInterface> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);

    const insertObj: ChatContext.MessageObjInterface = {
      id: GlobalContext.uuid4(),
      roomId,
      userId,
      message,
      createdAt: Date.now(),
      seenAt: null
    };
    await messageRepository.create(insertObj);
    if (this._errorClass.error) return;

    return insertObj;
  }

  /**
   * Update message
   * @param {string} id
   * @param {object} updateObj
   * @returns {Promise<ChatContext.MessageObjInterface>}
   */
  public async update(
    id: string,
    updateObj: object
  ): Promise<ChatContext.MessageObjInterface> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);
    const messageRes = await messageRepository.update(id, updateObj);
    if (this._errorClass.error) return;
    return messageRes;
  }

  /**
   * Find message by id
   * @param {string} id
   * @returns {Promise<ChatContext.MessageObjInterface>}
   */
  public async findById(id: string): Promise<ChatContext.MessageObjInterface> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);
    const userObj = await messageRepository.findById(id);
    if (this._errorClass.error) return;
    return userObj;
  }

  /**
   * Find message by targetUserId
   * @param {string} roomId
   * @returns {Promise<ChatContext.MessageObjInterface[]>}
   */
  public async findLastByRoomId(roomId: string): Promise<ChatContext.MessageObjInterface> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);
    const messagesRes = await messageRepository.findMany({roomId}, 'createdAt');
    if (this._errorClass.error) return;
    if (!messagesRes) return;
    messagesRes.reverse();
    return messagesRes[0] || null;
  }

  /**
   * Find message by targetUserId
   * @param {string} roomId
   * @param {number} [watermark=undefined]
   * @param {number} [offset=0]
   * @param {number} [limit=10]
   * @returns {Promise<ChatContext.MessageObjInterface[]>}
   */
  public async findManyByRoomId(
    roomId: string,
    watermark: number = undefined,
    offset: number = 0,
    limit: number = 0
  ): Promise<ChatContext.MessageObjInterface[]> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);
    let messagesRes = await messageRepository.findMany({roomId}, 'createdAt', limit, watermark);
    if (this._errorClass.error) return;
    if (offset) {
      messagesRes = messagesRes.reverse().filter((_, index: number) => index >= offset).reverse();
    }
    if (limit) {
      messagesRes = messagesRes.reverse().filter((_, index: number) => index < limit).reverse();
    }
    return messagesRes;
  }

  /**
   * Count messages for roomId
   * @param {string} roomId
   * @param {number} [watermark=undefined]
   * @returns {Promise<number>}
   */
  public async countByRoomId(roomId: string, watermark: number = undefined): Promise<number> {
    if (this._errorClass.error) return;
    const messageRepository = new ChatContext.MessageRepository(this._errorClass);
    return await messageRepository.count({roomId}, watermark);
  }
}