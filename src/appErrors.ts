import { Application, Response, Request, NextFunction } from 'express';
/**
 *
 */
export default (app: Application) => {
  /**
   * Error handler
   */
  app.use((req: Request, res: Response, next) => {
    const err: any = new Error('Not Found');
    err.status = 404;
    next(err);
  });
  if (process.env.NODE_ENV === 'production') {
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      res.json({
        error: true
      });
    });
  } else {
    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
      res.json({
        error: err.message
      });
    });
  }
};