import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import cors from 'cors';

/**
 * Routes import
 */
import externalRouter from './externalRouter';

/**
 * Initialize Express
 */
const app: express.Application = express();

/**
 * Config Express
 */
app.enable('trust proxy');
app.set('x-powered-by', false);
app.disable('x-powered-by');

/**
 * Middleware
 */
app.use( cookieParser() );
app.use( methodOverride() );
app.use( cors({origin: true, credentials: true}) );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true }) );

/**
 * Routes initialize
 */
app.use(externalRouter);

/**
 * Export
 */
export default app;