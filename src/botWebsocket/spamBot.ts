import faker from 'faker/locale/ru';
import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

const usersForBotHash: {[key: string]: NodeJS.Timeout} = {};

export default (socket: InfrastructureContext.WebsocketInterface = undefined, roomObj: ChatContext.RoomObjInterface = undefined, disconnect: boolean = false) => {
  // Disconnect user from spam bot
  if (disconnect) {
    if (usersForBotHash[socket.user.id]) clearTimeout(usersForBotHash[socket.user.id]);
    return;
  }

  // Clear before timeout
  if (usersForBotHash[socket.user.id]) clearTimeout(usersForBotHash[socket.user.id]);

  // Function of logic
  const spamBotLogic = async () => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const messageService = new ChatContext.MessageService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };

    // block: message_save
    try {
      const messageRes: ChatContext.MessageObjInterface = await messageService.create(roomObj.id, process.env.CHAT_BOT_SPAM_ID, faker.lorem.words(3));
      if (messageRes) {
        returnObj.data = messageRes;
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/roomMessageSend',
        'logger.description': 'block: message_save',
        'logger.error': err
      });
      errorClass.setError('block: message_save', 'unknown error in block message_save', '');
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }

    socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_RECEIVE, returnObj);

    clearTimeout(usersForBotHash[socket.user.id]);
    usersForBotHash[socket.user.id] = setTimeout(spamBotLogic, faker.random.number({min: 10, max: 120}) * 1000);
  };

  // Connect
  usersForBotHash[socket.user.id] = setTimeout(spamBotLogic, faker.random.number({min: 10, max: 120}) * 1000);
};