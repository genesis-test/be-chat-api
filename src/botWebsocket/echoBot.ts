import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default async (socket: InfrastructureContext.WebsocketInterface = undefined, data: any) => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const messageService = new ChatContext.MessageService(errorClass);

  /**
   * Variables
   */
  const returnObj: InfrastructureContext.WebsocketResponse = {
    data: null,
    status: true
  };
  const userId: string = data.targetUserId;
  data.targetUserId = socket.user.id;

  // block: message_save
  try {
    const messageRes: ChatContext.MessageObjInterface = await messageService.create(data.roomId, userId, data.message);
    if (messageRes) {
      returnObj.data = messageRes;
    }
  } catch (err) {
    InfrastructureContext.LoggerService.fatal({
      'logger.type': 'script',
      'logger.scriptName': 'externalWebsocket/roomMessageSend',
      'logger.description': 'block: message_save',
      'logger.error': err
    });
    errorClass.setError('block: message_save', 'unknown error in block message_save', '');
  }

  // Generate websocket response
  if (errorClass.error) {
    returnObj.status = false;
    returnObj.error = errorClass.errorsArr[0].description;
  }

  socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_RECEIVE, returnObj);

};