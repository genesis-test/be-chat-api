
import * as InfrastructureContext from './infrastructureContext';
import * as ChatContext from './chatContext';

export default async () => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const userService = new ChatContext.UserService(errorClass);
  const lowDB: InfrastructureContext.LowdbClient = InfrastructureContext.LowdbService.clientStatic;
  /**
   * Create empty collections
   */
  lowDB.defaults({
    [process.env.LOWDB_USERS_COLLECTION]: [],
    [process.env.LOWDB_ACCESS_TOKENS_COLLECTION]: [],
    [process.env.LOWDB_ROOMS_COLLECTION]: [],
    [process.env.LOWDB_MESSAGES_COLLECTION]: []
  }).write();
  /**
   * Install bots
   */
  if (!errorClass.error) {
    await userService.createBot(
      process.env.CHAT_BOT_ECHO_ID,
      'Echo Bot',
      '/test-data/avatar-bot-1.png',
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
    );
  }
  if (!errorClass.error) {
    await userService.createBot(
      process.env.CHAT_BOT_REVERSE_ID,
      'Reverse Bot',
      '/test-data/avatar-bot-2.png',
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
    );
  }
  if (!errorClass.error) {
    await userService.createBot(
      process.env.CHAT_BOT_SPAM_ID,
      'Spam Bot',
      '/test-data/avatar-bot-3.png',
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
    );
  }
  if (!errorClass.error) {
    await userService.createBot(
      process.env.CHAT_BOT_IGNORE_ID,
      'Ignore Bot',
      '/test-data/avatar-bot-4.png',
      'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
    );
  }
};