import { Response, Router } from 'express';
import authRoutes from './auth';

/**
 * Initialize Express Router
 */
const router = Router({ mergeParams: true });

/**
 * Initialize root endpoint for API
 */
router.get('/', (_, res: Response) => res.status(200).json({
  title: 'Chat API',
  version: process.env.API_VERSION
}));

/**
 * Initialize all endpoints
 */
router.use('/auth', authRoutes);

/**
 * Export
 */
export default router;