import { Router } from 'express';
import loginPOST from './loginPOST';
import loginTokenPOST from './loginTokenPOST';
import signUpPOST from './signUpPOST';

/**
 * Initialize Express Router
 */
const router = Router();

/**
 * Initialize all endpoints
 */
router.post('/login', loginPOST);
router.post('/login/token', loginTokenPOST);
router.post('/signup', signUpPOST);

/**
 * Export
 */
export default router;