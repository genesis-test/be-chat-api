import { Response, Request } from 'express';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '../../chatContext';

export default async (req: Request, res: Response) => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const userService = new ChatContext.UserService(errorClass);
  const accessTokenService = new ChatContext.AccessTokenService(errorClass);

  // Variables
  const returnObj: InfrastructureContext.APIResponse = {
    data: {user: null, token: null},
    status: true
  };
  const headerAuthorization = req.get('Authorization');
  let token: string;
  let tokenObj: ChatContext.AccessTokenObjInterface;
  let userRes: ChatContext.UserAPIInterface;

  // block: token_validate_and_load
  try {
    token = headerAuthorization.split(' ')[1];
    const tokenRes = await accessTokenService.validate(token);
    if (tokenRes) {
      returnObj.data.token = tokenObj = tokenRes;
    }
  } catch (err) {
    InfrastructureContext.LoggerService.fatal({
      'logger.type': 'script',
      'logger.scriptName': 'externalRouter/auth/loginTokenPOST',
      'logger.description': 'block: token_validate_and_load',
      'logger.error': err
    });
    errorClass.setError('block: token_validate_and_load', 'unknown error in block token_validate_and_load', '');
  }

  // block: user_load
  if (!errorClass.error && tokenObj) {
    try {
      userRes = await userService.findById(tokenObj.userId);
      if (!errorClass.error && userRes) {
        returnObj.data.user = userRes;
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalRouter/auth/loginTokenPOST',
        'logger.description': 'block: user_load',
        'logger.error': err
      });
      errorClass.setError('block: user_load', 'unknown error in block user_load', '');
    }
  }

  /**
   * Endpoint response
   * TODO for normal information for errors
   */
  if (errorClass.error) {
    returnObj.status = false;
    returnObj.error = errorClass.errorsArr[0].description;
  }
  res.status(200).json(returnObj);
};