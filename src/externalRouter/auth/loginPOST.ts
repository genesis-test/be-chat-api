import { Response, Request } from 'express';
import * as InfrastructureContext from '../../infrastructureContext';
import * as ChatContext from '../../chatContext';

export default async (req: Request, res: Response) => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const userService = new ChatContext.UserService(errorClass);
  const accessTokenService = new ChatContext.AccessTokenService(errorClass);

  // Variables
  const returnObj: InfrastructureContext.APIResponse = {
    data: {user: null, token: null},
    status: true
  };
  const bodyObj = req.body;
  let bValidate: boolean = false;
  let userRes: ChatContext.UserAPIInterface;

  // block: validate_data
  try {
    bValidate = await userService.loginValidate(bodyObj.email, bodyObj.password);
  } catch (err) {
    InfrastructureContext.LoggerService.fatal({
      'logger.type': 'script',
      'logger.scriptName': 'externalRouter/auth/loginPOST',
      'logger.description': 'block: validate_data',
      'logger.error': err
    });
    errorClass.setError('block: validate_data', 'unknown error in block validate_data', '');
  }

  // block: user_load
  if (!errorClass.error && bValidate) {
    try {
      userRes = await userService.findByEmailAndPassword(bodyObj.email, bodyObj.password);
      if (!errorClass.error && userRes) {
        returnObj.data.user = userRes;
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalRouter/auth/loginPOST',
        'logger.description': 'block: user_load',
        'logger.error': err
      });
      errorClass.setError('block: user_load', 'unknown error in block user_load', '');
    }
  }

  // block: generate_access_token
  if (!errorClass.error) {
    try {
      const tokenObj = await accessTokenService.create(userRes.id);
      if (!errorClass.error && tokenObj) {
        returnObj.data.token = tokenObj.token;
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalRouter/auth/loginPOST',
        'logger.description': 'block: generate_access_token',
        'logger.error': err
      });
      errorClass.setError('block: generate_access_token', 'unknown error in block generate_access_token', '');
    }
  }

  /**
   * Endpoint response
   * TODO for normal information for errors
   */
  if (errorClass.error) {
    returnObj.status = false;
    returnObj.error = errorClass.errorsArr[0].description;
  }
  res.status(200).json(returnObj);
};