import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default (socket: InfrastructureContext.WebsocketInterface = undefined) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const messageService = new ChatContext.MessageService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: [],
      status: true
    };
    let messagesCount: number;

    // block: messages_count
    try {
      messagesCount = await messageService.countByRoomId(data.roomId);
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/messagesLoad',
        'logger.description': 'block: messages_count',
        'logger.error': err
      });
      errorClass.setError('block: messages_count', 'unknown error in block messages_count', '');
    }

    // block: messages_load
    try {
      const messagesRes: ChatContext.MessageObjInterface[] = await messageService.findManyByRoomId(data.roomId, data.watermark, data.offset, 10);
      if (messagesRes) {
        if (messagesRes.length) {
          returnObj.data = {
            messages: messagesRes,
            more: messagesCount > data.offset + 10
          };
        } else {
          returnObj.data = {
            messages: [],
            more: false
          };
        }
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/messagesLoad',
        'logger.description': 'block: messages_load',
        'logger.error': err
      });
      errorClass.setError('block: messages_load', 'unknown error in block messages_load', '');
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }
    socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGES_LOAD, returnObj);
  };
};
