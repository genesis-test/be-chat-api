import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';
import spamBot from '../botWebsocket/spamBot';

export default (socket: InfrastructureContext.WebsocketInterface = undefined) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const roomService = new ChatContext.RoomService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };
    let roomRes: ChatContext.RoomObjInterface;

    // block: room_disconnect
    try {
      await roomService.disconnectUser(socket.user.id);
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/roomJoin',
        'logger.description': 'block: room_disconnect',
        'logger.error': err
      });
      errorClass.setError('block: room_disconnect', 'unknown error in block room_disconnect', '');
    }

    // block: room_connect
    if (!errorClass.error) {
      try {
        roomRes = await roomService.connectUser(socket.user.id, data.targetUserId);
      } catch (err) {
        InfrastructureContext.LoggerService.fatal({
          'logger.type': 'script',
          'logger.scriptName': 'externalWebsocket/roomJoin',
          'logger.description': 'block: room_connect',
          'logger.error': err
        });
        errorClass.setError('block: room_connect', 'unknown error in block room_connect', '');
      }
    }

    if (roomRes) {
      returnObj.data = roomRes;
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }
    socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_JOIN_ID, returnObj);

    // Logic for bots
    spamBot(socket, roomRes, true);
    if (data.targetUserId === process.env.CHAT_BOT_SPAM_ID) spamBot(socket, roomRes);
  };
};
