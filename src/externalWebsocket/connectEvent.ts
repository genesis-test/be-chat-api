import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';
import usersLoad from './usersLoad';
import usersLoadBroadcast from './usersLoadBroadcast';
import roomJoin from './roomJoin';
import userTyping from './userTyping';
import messagesLoad from './messagesLoad';
import roomMessageSend from './roomMessageSend';
import roomMessageSeen from './roomMessageSeen';
import spamBot from '../botWebsocket/spamBot';

export default async (socket: InfrastructureContext.WebsocketInterface) => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const userOnlineService = new ChatContext.UserOnlineService(errorClass);
  const roomService = new ChatContext.RoomService(errorClass);

  // Connect user
  userOnlineService.connectUser(socket.user.id, socket.id);

  // Users load
  socket.on(process.env.WEBSOCKET_REQUEST_USERS_LOAD, usersLoad(socket));

  // Join to room
  socket.on(process.env.WEBSOCKET_REQUEST_ROOM_JOIN, roomJoin(socket));

  // Typing to room
  socket.on(process.env.WEBSOCKET_REQUEST_ROOM_TYPING, userTyping(socket));

  // Messages load
  socket.on(process.env.WEBSOCKET_REQUEST_ROOM_MESSAGES_LOAD, messagesLoad(socket));

  // Message to room
  socket.on(process.env.WEBSOCKET_REQUEST_ROOM_MESSAGE_SEND, roomMessageSend(socket));

  // Message seen
  socket.on(process.env.WEBSOCKET_REQUEST_ROOM_MESSAGE_SEEN, roomMessageSeen(socket));

  // Broadcast for load user
  usersLoadBroadcast();

  // Disconnect user
  socket.on('disconnect', async () => {
    userOnlineService.disconnectUser(socket.user.id);
    await roomService.disconnectUser(socket.user.id);
    spamBot(socket, undefined, true);
    usersLoadBroadcast();
  });
};
