import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default async (socket: InfrastructureContext.WebsocketInterface, next: Function) => {
  const errorClass = new InfrastructureContext.ErrorClassEntity();
  const userService = new ChatContext.UserService(errorClass);
  const accessTokenService = new ChatContext.AccessTokenService(errorClass);

  const errorObj: InfrastructureContext.WebsocketErrorInterface = {
    error: null,
    description: null
  };
  const token: string = socket.handshake.query.token;
  let tokenObj: ChatContext.AccessTokenObjInterface;
  let userRes: ChatContext.UserAPIInterface;

  // token_validate_and_load
  try {
    const tokenRes = await accessTokenService.validate(token);
    if (!errorClass.error && tokenRes) {
      tokenObj = tokenRes;
      socket.token = token;
    } else {
      errorObj.error = 'token_not_valid';
      errorObj.description = 'token not valid';
      errorClass.setError('token_not_valid', 'token not valid', '');
    }
  } catch (err) {
    InfrastructureContext.LoggerService.fatal({
      'logger.type': 'script',
      'logger.scriptName': 'websocket',
      'logger.description': 'block: token_validate_and_load',
      'logger.error': err
    });
    errorClass.setError('block: token_validate_and_load', 'unknown error in block token_validate_and_load', '');
  }

  // block: user_load
  if (!errorClass.error && tokenObj) {
    try {
      userRes = await userService.findById(tokenObj.userId);
      if (!errorClass.error && userRes) {
        socket.user = userRes;
      } else {
        errorObj.error = 'user_not_found';
        errorObj.description = 'user not found';
        errorClass.setError('user_not_found', 'user not found', '');
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'websocket',
        'logger.description': 'block: user_load',
        'logger.error': err
      });
      errorClass.setError('block: user_load', 'unknown error in block user_load', '');
    }
  }

  // Answer of ok
  if (!errorClass.error) {
    return next();
  }
  // Answer if not ok
  if (!errorObj.error && errorClass.errorsArr[0]) {
    errorObj.error = errorClass.errorsArr[0].name;
    errorObj.description = errorClass.errorsArr[0].description;
  }
  return next(new Error(JSON.stringify(errorObj)));
};
