import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';
import echoBot from '../botWebsocket/echoBot';
import reverseBot from '../botWebsocket/reverseBot';

export default (socket: InfrastructureContext.WebsocketInterface = undefined) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const userOnlineService = new ChatContext.UserOnlineService(errorClass);
    const messageService = new ChatContext.MessageService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };

    // block: message_save
    try {
      const messageRes: ChatContext.MessageObjInterface = await messageService.create(data.roomId, socket.user.id, data.message);
      if (messageRes) {
        returnObj.data = messageRes;
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/roomMessageSend',
        'logger.description': 'block: message_save',
        'logger.error': err
      });
      errorClass.setError('block: message_save', 'unknown error in block message_save', '');
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }

    // Send message for user sender
    socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_RECEIVE, returnObj);

    // Send message for user target
    if (userOnlineService.usersHash[data.targetUserId]) {
      const socketId: string = userOnlineService.usersHash[data.targetUserId].socketId;
      InfrastructureContext.WebsocketService.websocket.to(socketId).emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_RECEIVE, returnObj);
    }

    // Logic for bots
    if (data.targetUserId === process.env.CHAT_BOT_ECHO_ID) echoBot(socket, data);
    if (data.targetUserId === process.env.CHAT_BOT_REVERSE_ID) reverseBot(socket, data);
  };
};
