import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default (socket: InfrastructureContext.WebsocketInterface = undefined) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const userOnlineService = new ChatContext.UserOnlineService(errorClass);
    const messageService = new ChatContext.MessageService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };

    // block: message_update
    try {
      const messageRes: ChatContext.MessageObjInterface = await messageService.update(data.messageId, {seenAt: Date.now()});
      if (messageRes) {
        returnObj.data = {
          messageId: data.messageId,
          roomId: data.roomId,
          set: {
            seenAt: messageRes.seenAt
          }
        };
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/roomMessageSeen',
        'logger.description': 'block: message_update',
        'logger.error': err
      });
      errorClass.setError('block: message_update', 'unknown error in block message_update', '');
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }

    // Send message for user sender
    socket.emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_UPDATE, returnObj);

    // Send message for user target
    if (userOnlineService.usersHash[data.targetUserId]) {
      const socketId: string = userOnlineService.usersHash[data.targetUserId].socketId;
      InfrastructureContext.WebsocketService.websocket.to(socketId).emit(process.env.WEBSOCKET_ANSWER_ROOM_MESSAGE_UPDATE, returnObj);
    }
  };
};
