import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default (socket: InfrastructureContext.WebsocketInterface) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const userOnlineService = new ChatContext.UserOnlineService(errorClass);
    const userService = new ChatContext.UserService(errorClass);
    const roomService = new ChatContext.RoomService(errorClass);
    const messageService = new ChatContext.MessageService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };
    let usersArr: ChatContext.UserAPIInterface[] = null;
    let roomsArr: ChatContext.RoomObjInterface[] = [];
    let messagesArr: ChatContext.MessageObjInterface[] = [];

    // block: users_load
    try {
      const usersRes = await userService.findAll();
      if (usersRes) usersArr = usersRes;
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/usersLoad',
        'logger.description': 'block: users_load',
        'logger.error': err
      });
      errorClass.setError('block: users_load', 'unknown error in block users_load', '');
    }

    // block: rooms_load
    if (!errorClass.error) {
      try {
        const promisesArr: Promise<ChatContext.RoomObjInterface>[] = [];
        usersArr.forEach((user: ChatContext.UserAPIInterface) => {
          promisesArr.push(
            roomService.findByUserIds(socket.user.id, user.id)
          );
        });
        const roomsRes = await Promise.all(promisesArr);
        if (roomsRes) roomsArr = roomsRes;
      } catch (err) {
        InfrastructureContext.LoggerService.fatal({
          'logger.type': 'script',
          'logger.scriptName': 'externalWebsocket/usersLoad',
          'logger.description': 'block: rooms_load',
          'logger.error': err
        });
        errorClass.setError('block: rooms_load', 'unknown error in block rooms_load', '');
      }
    }

    // block: last_message_load
    if (!errorClass.error) {
      try {
        const promisesArr: Promise<ChatContext.MessageObjInterface>[] = [];
        roomsArr.forEach((roomRes: ChatContext.RoomObjInterface) => {
          promisesArr.push(
            roomRes ? messageService.findLastByRoomId(roomRes.id) : null
          );
        });
        const messagesRes = await Promise.all(promisesArr);
        if (messagesRes) messagesArr = messagesRes;
      } catch (err) {
        InfrastructureContext.LoggerService.fatal({
          'logger.type': 'script',
          'logger.scriptName': 'externalWebsocket/usersLoad',
          'logger.description': 'block: last_message_load',
          'logger.error': err
        });
        errorClass.setError('block: last_message_load', 'unknown error in block last_message_load', '');
      }
    }

    // block: parse_users
    if (!errorClass.error) {
      try {
        returnObj.data = usersArr
          .map((userRes: ChatContext.UserAPIInterface, index: number) => {
            return {
              id: userRes.id,
              email: userRes.email,
              userName: userRes.userName,
              avatar: userRes.avatar,
              description: userRes.description,
              isBot: userRes.isBot,
              roomId: roomsArr[index] ? roomsArr[index].id : null,
              message: messagesArr[index] || null,
              online: userRes.isBot ? true : !!userOnlineService.usersHash[userRes.id]
            };
          });
      } catch (err) {
        InfrastructureContext.LoggerService.fatal({
          'logger.type': 'script',
          'logger.scriptName': 'externalWebsocket/usersLoad',
          'logger.description': 'block: parse_users',
          'logger.error': err
        });
        errorClass.setError('block: parse_users', 'unknown error in block parse_users', '');
      }
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }
    socket.emit(process.env.WEBSOCKET_ANSWER_USERS_LOAD, returnObj);
  };
};
