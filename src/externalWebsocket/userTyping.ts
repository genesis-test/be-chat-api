import * as InfrastructureContext from '../infrastructureContext';
import * as ChatContext from '../chatContext';

export default (socket: InfrastructureContext.WebsocketInterface = undefined) => {
  return async (data: any) => {
    const errorClass = new InfrastructureContext.ErrorClassEntity();
    const userOnlineService = new ChatContext.UserOnlineService(errorClass);

    // Variables
    const returnObj: InfrastructureContext.WebsocketResponse = {
      data: null,
      status: true
    };
    let socketId: string;

    // block: user_online_get
    try {
      if (userOnlineService.usersHash[data.targetUserId]) {
        socketId = userOnlineService.usersHash[data.targetUserId].socketId;
        returnObj.data = {roomId: data.roomId, status: data.status};
      }
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'script',
        'logger.scriptName': 'externalWebsocket/userTyping',
        'logger.description': 'block: user_online_get',
        'logger.error': err
      });
      errorClass.setError('block: user_online_get', 'unknown error in block user_online_get', '');
    }

    // Generate websocket response
    if (errorClass.error) {
      returnObj.status = false;
      returnObj.error = errorClass.errorsArr[0].description;
    }
    if (socketId) {
      InfrastructureContext.WebsocketService.websocket.to(socketId).emit(process.env.WEBSOCKET_ANSWER_ROOM_TYPING, returnObj);
    }
  };
};
