import * as InfrastructureContext from '../infrastructureContext';

export default async () => {
  // Broadcast send
  InfrastructureContext.WebsocketService.websocket.emit(process.env.WEBSOCKET_BROADCAST_USERS_LOAD, {});
};
