import lowdb from 'lowdb';

export interface LowdbConnectInterface {
  path: string;
  db: string;
}

export type LowdbClient = lowdb.LowdbAsync<any>;