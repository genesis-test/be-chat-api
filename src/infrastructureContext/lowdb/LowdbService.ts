import lowdb from 'lowdb';
import FileAsync from 'lowdb/adapters/FileAsync';
import * as InfrastructureContext from '..';

/**
 * Global cache for lowDB client
 * @type {InfrastructureContext.LowdbClient}
 */
let client: InfrastructureContext.LowdbClient;

/**
 * LowDB wrap for connection/disconnect
 * @class
 */
export class LowdbService {
  constructor() {}

  /**
   * Return client variable for static use
   * @returns {InfrastructureContext.LowdbClient}
   */
  static get clientStatic(): InfrastructureContext.LowdbClient {
    return client;
  }

  /**
   * Return client variable for class use
   * @returns {InfrastructureContext.LowdbClient}
   */
  get client(): InfrastructureContext.LowdbClient {
    return client;
  }

  /**
   * Return connection check
   * @returns {boolean}
   */
  public checkConnect(): boolean {
    return !!this.client;
  }

  /**
   * Connect to local file
   * @param {InfrastructureContext.LowdbConnectInterface} con
   * @returns {Promise<InfrastructureContext.LowdbClient>}
   */
  public async connect(con: InfrastructureContext.LowdbConnectInterface): Promise<InfrastructureContext.LowdbClient> {
    if (this.client) return this.client;

    try {
      const adapter = new FileAsync(`${con.path}/${con.db}`);
      client = await lowdb(adapter);
      return client;
    } catch (err) {
      InfrastructureContext.LoggerService.fatal({
        'logger.type': 'class_method',
        'logger.className': 'LowdbService',
        'logger.method': 'connect',
        'logger.error': err
      });
      return err;
    }
  }

  /**
   * Disconnect and clear cache
   * @returns {Promise<void>}
   */
  public disconnect(): Promise<void> {
    return new Promise(resolve => {
      client = undefined;
      resolve();
    });
  }
}