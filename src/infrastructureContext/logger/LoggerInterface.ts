export interface LoggerGlobalInterface {
  'logger.level'?: 'debug' | 'info' | 'warning' | 'error' | 'fatal';
  'logger.description'?: string;
  'logger.error'?: Error;
  'logger.data'?: object;
}

export interface LoggerClassMethodInterface extends LoggerGlobalInterface {
  'logger.type': 'class_method';
  'logger.className': string;
  'logger.method': string;
  'logger.action'?: string;
}

export interface LoggerScriptInterface extends LoggerGlobalInterface {
  'logger.type': 'script';
  'logger.scriptName': string;
  'logger.method'?: string;
  'logger.action'?: string;
}

export interface LoggerHTTPRequestInterface extends LoggerGlobalInterface {
  'logger.type': 'http_request';
  'req.method': string;
  'req.host': string;
  'req.path': string;
  'req.query': object;
  'req.params': object;
}

export interface LoggerHTTPResponseInterface extends LoggerGlobalInterface {
  'logger.type': 'http_response';
  'res.status': number;
}

export interface LoggerRepositoryRequestInterface extends LoggerGlobalInterface {
  'logger.type': 'repository_request';
  'query_name': number;
}

export type LoggerMessageObjType = LoggerClassMethodInterface | LoggerScriptInterface | LoggerHTTPRequestInterface | LoggerHTTPResponseInterface | LoggerRepositoryRequestInterface;