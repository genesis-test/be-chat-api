import * as InfrastructureContext from '../../infrastructureContext';

/**
 * Global class for logging
 * @class
 */
export class LoggerService {
  constructor() {}

  /**
   * Debug log
   * @param {InfrastructureContext.LoggerMessageObjType} messageObj
   * @returns {void}
   */
  public static debug(messageObj: InfrastructureContext.LoggerMessageObjType): void {
    messageObj['logger.level'] = 'debug';
    console.log(JSON.stringify(messageObj));
  }

  /**
   * Info log
   * @param {InfrastructureContext.LoggerMessageObjType} messageObj
   * @returns {void}
   */
  public static info(messageObj: InfrastructureContext.LoggerMessageObjType) {
    messageObj['logger.level'] = 'info';
    console.log(JSON.stringify(messageObj));
  }

  /**
   * Warning log
   * @param {InfrastructureContext.LoggerMessageObjType} messageObj
   * @returns {void}
   */
  public static warning(messageObj: InfrastructureContext.LoggerMessageObjType) {
    messageObj['logger.level'] = 'warning';
    console.log(JSON.stringify(messageObj));
  }

  /**
   * Error log
   * @param {InfrastructureContext.LoggerMessageObjType} messageObj
   * @returns {void}
   */
  public static error(messageObj: InfrastructureContext.LoggerMessageObjType) {
    messageObj['logger.level'] = 'error';
    console.log(JSON.stringify(messageObj));
  }

  /**
   * Fatal log
   * @param {InfrastructureContext.LoggerMessageObjType} messageObj
   * @returns {void}
   */
  public static fatal(messageObj: InfrastructureContext.LoggerMessageObjType) {
    messageObj['logger.level'] = 'fatal';
    console.log(JSON.stringify(messageObj));
  }
}