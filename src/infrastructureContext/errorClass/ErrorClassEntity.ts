import * as InfrastructureContext from '..';

/**
 * Global class for error state
 * @class
 */
export class ErrorClassEntity {
  /**
   * @param {boolean} _error true — when error happened, false — normal
   */
  protected _error: boolean;
  /**
   * @param {InfrastructureContext.ErrorClassItemInterface} _errorsArr array of errors
   */
  protected _errorsArr: InfrastructureContext.ErrorClassItemInterface[];

  constructor() {
    this._error = false;
    this._errorsArr = [];
  }

  /**
   * Return error status
   * @returns {boolean}
   */
  get error(): boolean { return this._error; }

  /**
   * Return array errors
   * @returns {InfrastructureContext.ErrorClassItemInterface[]}
   */
  get errorsArr(): InfrastructureContext.ErrorClassItemInterface[] { return this._errorsArr; }

  /**
   * Add error
   * @param {string} name
   * @param {string} description
   * @param {string} [code=undefined]
   * @param {Error} [err=undefined]
   * @returns {void}
   */
  setError(name: string, description: string, code: string = undefined, err: Error = undefined): void {
    this._error = true;
    this._errorsArr.push({
      name,
      description,
      code,
      err
    });
  }

  /**
   * Reset all errors
   * @returns {void}
   */
  reset(): void {
    this._error = false;
    this._errorsArr = [];
  }
}