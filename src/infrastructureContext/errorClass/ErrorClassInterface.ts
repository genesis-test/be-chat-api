export interface ErrorClassItemInterface {
  name: string;
  description: string;
  code: string;
  err?: Error;
}