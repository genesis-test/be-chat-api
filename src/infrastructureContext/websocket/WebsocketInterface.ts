import socketIO from 'socket.io';
import * as ChatContext from '../../chatContext';

export interface WebsocketErrorInterface {
  error: string;
  description: string;
}

export type WebsocketServerType = socketIO.Server;

export interface WebsocketInterface extends socketIO.Socket {
  user: ChatContext.UserAPIInterface;
  token: string;
}

export interface WebsocketResponse {
  status: boolean;
  data: any;
  error?: string;
}