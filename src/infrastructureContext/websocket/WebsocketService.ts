import * as InfrastructureContext from '..';

/**
 * Global cache for websocket
 * @type {InfrastructureContext.WebsocketServerType}
 */
let websocket: InfrastructureContext.WebsocketServerType;

/**
 * Websocket class
 * @class
 */
export class WebsocketService {

  constructor() {}

  /**
   * Websocket server get from global cache
   * @returns {InfrastructureContext.WebsocketServerType} websocketTmp
   */
  public static get websocket(): InfrastructureContext.WebsocketServerType {
    return websocket;
  }

  /**
   * Websocket server set to global cache
   * @param {InfrastructureContext.WebsocketServerType} websocketTmp
   */
  public static websocketSet(websocketTmp: InfrastructureContext.WebsocketServerType): void {
    websocket = websocketTmp;
  }
}