export interface APIResponse {
  status: boolean;
  data: any;
  error?: string;
}