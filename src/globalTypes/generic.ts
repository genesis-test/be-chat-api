export type OptionalizeType<T> = {
  [K in keyof T]?: T[K]
};

export type PromisifyType<T> = {
  [K in keyof T]?: Promise<T[K]>
};