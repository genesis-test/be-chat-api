export type ObjectType = {[key: string]: any};
export type ObjectBooleanType = {[key: string]: boolean};
export type ObjectStringType = {[key: string]: string};
export type ObjectNumberType = {[key: string]: number};
export type ObjectScyllaType = {[key: string]: string};
